import { baseUrl } from './config';
import { userId, userToken } from '../scripts/user';

async function registerGroup(institution: string, name: string): Promise<[number, any]> {
	const response = await fetch(baseUrl + 'createGroup', {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			Authorization: userToken,
			'Client-Id': userId
		},
		body: JSON.stringify({ institution: institution, name: name })
	}).then(async function (res) {
		return [res.status, await res.json()];
	});

	//		http code	 response json body
	return [response[0], response[1]];
}

async function deleteGroup(id: string): Promise<[number, any]> {
	const response = await fetch(baseUrl + 'deleteGroup', {
		method: 'DELETE',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			Authorization: userToken,
			'Client-Id': userId
		},
		body: JSON.stringify({ id: id })
	}).then(async function (res) {
		return [res.status, await res.json()];
	});

	//		http code	 response json body
	return [response[0], response[1]];
}

export { registerGroup, deleteGroup };
