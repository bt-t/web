import { baseUrl } from './config';
import { writable } from 'svelte/store';
let userId: string;
let loggedIn: boolean;
let userToken: string;

export const logInEvent = writable(false);

async function signUp(
	username: string,
	email: string,
	password: string
): Promise<[number, string]> {
	const response = await fetch(baseUrl + 'signup', {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({ email: email, username: username, password: password })
	}).then(async function (res) {
		return [res.status, await res.json()];
	});

	//		http code	 response json body
	return [response[0], response[1]];
}

async function signIn(email: string, password: string): Promise<number> {
	const response = await fetch(`${baseUrl}login?email=${email}&password=${password}`, {
		method: 'GET',
		headers: {
			Accept: 'application/json'
		}
	}).then(async function (res) {
		return [res.status, await res.json()];
	});

	userId = response[1].id;
	userToken = response[1].token;
	return response[0];
}

function setCreds(uid: string, utoken: string): void {
	userId = uid;
	userToken = utoken;
}

async function getUser(uid: string, utoken: string): Promise<[number, any]> {
	const response = await fetch(baseUrl + 'getUser', {
		method: 'GET',
		headers: {
			Accept: 'application/json',
			Authorization: utoken,
			'Client-Id': uid
		}
	}).then(async function (res) {
		return [res.status, await res.json()];
	});

	//		http code	 status text from json
	return [response[0], response[1]];
}

export function setLoginState(state: boolean): void {
	loggedIn = state;
	logInEvent.update((n) => true);
}

export { signUp, signIn, getUser, userId, userToken, setCreds, loggedIn };
