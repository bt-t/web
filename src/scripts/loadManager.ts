import { writable } from 'svelte/store';

export const loading = writable(false);

export function setLoading(state: boolean) {
	loading.update((n) => state);
}
