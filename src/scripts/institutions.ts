import { baseUrl } from './config';
import { userId, userToken } from './user';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function getInst(instId: string, instName: string): Promise<[number, any]> {
	let url = baseUrl + 'getInst';
	if (instId != '') {
		url = `${url}?institutionId=${instId}`;
	} else if (instName != '') {
		url = `${url}?institutionName=${instName}`;
	} else {
		throw 'institution name and id missing!';
	}

	const response = await fetch(url, {
		method: 'GET',
		headers: {
			Accept: 'application/json'
		}
	}).then(async function (res) {
		return [res.status, await res.json()];
	});

	return [response[0], response[1]];
}

async function registerInst(instName: string): Promise<[number, any]> {
	const url = baseUrl + 'createInstitution';

	const response = await fetch(url, {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			Authorization: userToken,
			'Client-Id': userId
		},
		body: JSON.stringify({ name: instName })
	}).then(async function (res) {
		return [res.status, await res.json()];
	});

	console.log(response[0], response[1]);

	return [response[0], response[1]];
}

export { getInst, registerInst };
