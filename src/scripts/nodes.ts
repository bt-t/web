import { baseUrl } from './config';
import { userId, userToken } from '../scripts/user';

async function registerNode(group: string, name: string): Promise<[number, any]> {
	const response = await fetch(baseUrl + 'createNode', {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			Authorization: userToken,
			'Client-Id': userId
		},
		body: JSON.stringify({ group: group, name: name })
	}).then(async function (res) {
		return [res.status, await res.json()];
	});

	//		http code	 response json body
	return [response[0], response[1]];
}

async function deleteNode(id: string): Promise<[number, any]> {
	const response = await fetch(baseUrl + 'deleteNode', {
		method: 'DELETE',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			Authorization: userToken,
			'Client-Id': userId
		},
		body: JSON.stringify({ id: id })
	}).then(async function (res) {
		return [res.status, await res.json()];
	});

	//		http code	 response json body
	return [response[0], response[1]];
}

export { registerNode, deleteNode };
