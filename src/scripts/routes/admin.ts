import * as user from '../user';
import { setLoading } from '../loadManager';
import 'svelte';

//USER DATA
let nodes = [];
let groups = [];
let institutions = [];

export { nodes, groups, institutions };

export async function setData(uid: string, token: string): Promise<boolean> {
	const data = await user.getUser(uid, token);

	if (data[0] != 200) {
		return false;
	}

	institutions = data[1].institutions;
	groups = data[1].groups;
	nodes = data[1].nodes;
	return true;
}

export async function attemptCookieLogin(): Promise<void> {
	if (document.cookie != '') {
		setLoading(true);

		//Get and parse cookies
		const cookie = document.cookie
			.split(';')
			.map((v) => v.split('='))
			.reduce((acc, v) => {
				acc[decodeURIComponent(v[0].trim())] = decodeURIComponent(v[1].trim());
				return acc;
			}, {});

		if (!(await setData(cookie['userId'], cookie['userToken']))) {
			setLoading(false);
			document.cookie = `userId=`;
			document.cookie = `userToken=`;
			return;
		}

		user.setCreds(cookie['userId'], cookie['userToken']);
		setLoading(false);
		console.log('Logged in using cookie');
		user.setLoginState(true);
	}
}
