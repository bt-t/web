FROM node:17 as build-env

WORKDIR /usr/src/app

COPY package*.json ./

RUN echo "THIS MAY TAKE A WHILE, DO NOT CANCEL IF IT HANGS FOR A FEW MINUTES" && npm install

COPY ./src ./src
COPY ./static ./static
COPY ./svelte.config.js ./svelte.config.js
COPY ./tsconfig.json ./tsconfig.json

RUN npm run css-build
RUN npm run build

# Run Stage
FROM node:17-alpine

WORKDIR /usr/app
 
# Set environment variable
ENV APP_NAME btt-web
 
# Copy only required data into this image
COPY --from=build-env /usr/src/app/package*.json ./
COPY --from=build-env /usr/src/app/build ./build

EXPOSE 80

ENV PORT=80
ENV HOST=0.0.0.0

CMD [ "npm", "run", "host" ]